from django.shortcuts import render
from update_status.models import Todo
from Add_Friend.models import ListFriend

# Create your views here.
response = {'author': "syuvira"}

def index(request):
    html = 'dashboard/dashboard.html'
    response['statusCount'] = Todo.objects.all().count()
    response['friendsCount'] = ListFriend.objects.all().count()
    response['latestPost'] = Todo.objects.last()
    return render(request, html, response)



"""Project1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import update_status.urls as update_status
import profile_page.urls as profile_page
import Add_Friend.urls as Add_Friend
import dashboard.urls as dashboard
import welcome_page.urls as welcome_page

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^update-status/', include(update_status, namespace='update-status')),
	url(r'^profile-page/', include(profile_page, namespace='profile-page')),
	url(r'^Add-Friend/', include(Add_Friend, namespace='Add-Friend')),
	url(r'^dashboard/', include(dashboard, namespace='dashboard')),
	url(r'^$', include(welcome_page, namespace='welcome-page')),
]

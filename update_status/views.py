from django.shortcuts import render
from django.db.utils import OperationalError
from django.http import HttpResponseRedirect
from .models import Todo
from .forms import Todo_Form


# Create your views here.

response = {}

def index(request):
	response['author'] = "syuvira" #TODO Implement yourname
	todo = Todo.objects.all()
	response['todo'] = todo
	html = 'update_status/update_status.html'
	response['todo_form'] = Todo_Form
	return render(request, html, response)

	
	
def add_todo(request):
    form = Todo_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['description'] = request.POST['description']
        todo = Todo(description=response['description'])
        todo.save()
        return HttpResponseRedirect('/update-status/')
    else:
        return HttpResponseRedirect('/update-status/')

		
def del_status(request):
    if(request.method == 'POST'):
        del_id_btn = request.POST['del_id']
        Todo.objects.filter(id=del_id_btn).delete()
        return HttpResponseRedirect('/update-status/')
    else:
        return HttpResponseRedirect('/update-status/')

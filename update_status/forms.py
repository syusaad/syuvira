from django import forms


class Todo_Form(forms.Form):
	error_messages = {
		'required': 'This field is required!',
		
		}
		
		
	description_attrs = {
	'type': 'text',
	'cols': 100,
	'rows': 4,
	'class': 'todo-form-textarea',
	'placeholder': "what's on your mind?..."
		
	}
	
	description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))
	
	
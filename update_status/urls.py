from django.conf.urls import url
from .views import index, add_todo, del_status

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^add_todo', add_todo, name='add_todo'),
	url(r'del-status/$', del_status, name='del-status'),
]

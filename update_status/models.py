from django.db import models


# Create your models here.

class Todo(models.Model):
	description = models.TextField()
	created_date = models.DateTimeField(auto_now_add=True)
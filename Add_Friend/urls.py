from django.conf.urls import url
from .views import index
from .views import add_friend
from .views import del_friend



urlpatterns = [
url(r'^$', index, name='index'),
url(r'add-friend', add_friend, name='add-friend'),
url(r'del-friend/$', del_friend, name='del-friend'),

        
]

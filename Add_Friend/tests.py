from django.test import TestCase,Client
from django.urls import resolve
from .views import index, add_friend
from .models import ListFriend
from .forms import add_form

class AddFriendTest(TestCase):

        def test_Add_Friend_url_is_exist(self):
            response = Client().get('/Add-Friend/')
            self.assertEqual(response.status_code,200)

        def test_AddFriend_using_index_func(self):
            found = resolve('/Add-Friend/')
            self.assertEqual(found.func, index)

        def test_add_friend_input_has_placeholder_and_css_classes(self):
            form = add_form()
            self.assertIn('class="add-form-input', form.as_p())
            self.assertIn('id="id_Name"', form.as_p())
            self.assertIn('class="add-form-textarea', form.as_p())
            self.assertIn('id="id_URL', form.as_p())    
                
        def test_form_validation_for_blank_items(self):
            form = add_form(data={'Name': '', 'URL': ''})
            self.assertFalse(form.is_valid())
            self.assertEqual(
                form.errors['URL'],
                ["This field is required."]
            )
        def test_model_can_add_new_friend(self):
                                        #Creating a new friend
                                        new_friend = ListFriend.objects.create(Name='friends name',URL='heroku link')

                                        #Retrieving friends list
                                        counting_all_available_friends = ListFriend.objects.all().count()
                                        self.assertEqual(counting_all_available_friends,1)
                        
                        
        def test_AddFriend_post_success_and_render_the_result(self):
            test = 'Anonymous'
            response_post = Client().post('/Add-Friend/add-friend', {'Name': test, 'URL': test})
            self.assertEqual(response_post.status_code, 302)

            response= Client().get('/Add-Friend/')
            html_response = response.content.decode('utf8')
            self.assertIn(test, html_response)
                        
        def test_AddFriend_post_error_and_render_the_result(self):
            test = 'Anonymous'
            response_post = Client().post('/Add-Friend/add-friend', {'Name': '', 'URL': ''})
            self.assertEqual(response_post.status_code, 302)

            response= Client().get('/Add-Friend/')
            html_response = response.content.decode('utf8')
            self.assertNotIn(test, html_response)

      

from django import forms

class add_form(forms.Form):
    error_messages = {
        'required': 'Please fill this input',
    }
    Name_attrs = {
        'type': 'text',
        'class': 'add-form-input',
        'placeholder':'Enter name'
		
    }
    URL_attrs = {
        'type': 'URL',
        'cols': 50,
        'rows': 4,
        'class': 'add-form-textarea',
        'placeholder':'Enter herokuapp link'
		
    }

    Name = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=Name_attrs))
    URL = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=URL_attrs))

	

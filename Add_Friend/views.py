from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import ListFriend
from .forms import add_form

# Create your views here.
#List_Friend = {}
response={}
def index(request):
    response['author'] = "Tarsvini"
    listfriend = ListFriend.objects.all()
    response['listfriend'] =listfriend
    html = 'Add_Friend/add_friend.html'
    response['add_form'] = add_form
    return render(request, html, response)
   
	
def add_friend(request):
    form = add_form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()) :
        response['Name'] = request.POST['Name']
        response['URL'] = request.POST['URL']
        listfriend = ListFriend(Name=response['Name'],URL=response['URL'])
        listfriend.save()
        return HttpResponseRedirect('/Add-Friend/')
    else:
        return HttpResponseRedirect('/Add-Friend/')
    

		
def del_friend(request):
    if(request.method == 'POST'):
        del_id_btn = request.POST['del_id']
        ListFriend.objects.filter(id=del_id_btn).delete()
        return HttpResponseRedirect('/Add-Friend/')
    else:
        return HttpResponseRedirect('/Add-Friend/')





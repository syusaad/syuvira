 
CSGE602022 - Web Design & Programming (Perancangan & Pemrograman Web) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2017/2018

* * *
PIPELINES STATUS : [![pipeline status](https://gitlab.com/syusaad/syuvira/badges/master/pipeline.svg)](https://gitlab.com/syusaad/syuvira/commits/master)


COVERAGE REPORT : [![coverage report](https://gitlab.com/syusaad/syuvira/badges/master/coverage.svg)](https://gitlab.com/syusaad/syuvira/commits/master)



ASSIGNMENT (project 1)

GROUP NAME : SYUVIRA

GROUP MEMBERS:
1. Tarsvini Ravinther
2. Rossalmira Abdul Rahman
3. Syuhadah Binti Muhamed Sa'ad

			 
Heroku Apps : (https://syuvira.herokuapp.com/)

            
			 "You Learn More From Failure Than From Success. Don't Let It Stop You. Failure Builds Character."- Unknown


FEATURES
1. update_status: Rossalmira Abdul Rahman
2. profile_page : Syuhadah Binti Muhamed Sa'ad
3. Add_Friend   : Tarsvini Ravinther
4. dashboard    : Everyone

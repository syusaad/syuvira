from django.shortcuts import render
from datetime import date

name = 'Syuvira' # TODO Implement this
birthday = date(1996, 10, 13) #TODO Implement this, format (Year, Month, Date)
gender = 'Female'
expertise = ["Arts", "Computer", "Sleep"]
description = 'Cat-lover, wanderlust'
email = 'syuvira@whatever.com'

def index(request):
   response = {'name': name, 'birthday': birthday, 'gender': gender, 'expertise': expertise, 'description': description, 'email': email}
   html = 'profile_page/profile_page.html'
   return render(request, html, response)

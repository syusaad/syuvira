from django.db import models
from django.shortcuts import get_object_or_404

class Expertise(models.Model):
    expertise = models.TextField()

class Profile_Page(models.Model):
    name = models.TextField()
    birthday = models.DateField()
    gender = models.TextField()
    expertise = models.ManyToManyField(Expertise)
    description = models.TextField()
    email = models.TextField

    def __str__(self):
        return self.name

    def getBirthday(self):
        return self.birthday

    def getGender(self):
        return self.gender

    def getExpertise(self):
        return self.expertise

    def getDescription(self):
        return self.description

    def getEmail(self):
        return self.email




   